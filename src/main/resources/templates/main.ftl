<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="Wong Shek Yoon" content="">
 
    <title>${title}</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/codingtests.css" rel="stylesheet">
  </head>
<body>
	<nav class="navbar navbar-dark bg-dark">
	  <span class="navbar-brand mb-0 h1">${title}</span>
	</nav>
	
    <div class="container-fluid">
      <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
          <ul class="nav nav-pills flex-column" id="navModules">
            <li class="nav-item">
              <a class="nav-link active" href="#" id="navOverview">Overview <span class="sr-only">(current)</span></a>
            </li>
    
            <li class="nav-item" >
              <a class="nav-link" href="#" id="navUtilities">Utilities</a>
            </li>           
          </ul>
        </nav>

        <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
          <div class="container" id="divContent">
            
          </div>
        </main>
      </div>
    </div>

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/codingtests.js"></script>
</body>

</html>
