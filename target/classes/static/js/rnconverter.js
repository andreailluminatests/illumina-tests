$(function() {	 
	   
    $("#tabRNConverter").click(function(){
    	rnconverter.loadRnConverter();
    });
    
    $("#btnRnConvert").click(function(){
    	$("#divMessage").hide();
    	rnconverter.doRnConversion();
    });
    
    $("#rnInput").on('change keyup paste', function() {     
    	$("#numericalInput").val("");
    });
    
    $("#numericalInput").on('change keyup paste', function() {
    	$("#rnInput").val("");
    });
    
    $("#divClearNumber").click(function(){
    	$("#numericalInput").val("");
    });
    
    $("#divClearRoman").click(function(){
    	$("#rnInput").val("");
    });
    
    $("#btnReset").click(function(){
    	$("#divMessage").hide();
    	$("#numericalInput").val("");
    	$("#rnInput").val("");
    });
});

var rnconverter = {
	doRnConversion : function ()
	{
		var romanNumerical = $("#rnInput").val();
		var numericalValue = $("#numericalInput").val();

		if (typeof romanNumerical !== "undefined" && romanNumerical)
		{    
			$("#numericalInput").val(this.Roman2Numerical(romanNumerical));
		}else if (typeof numericalValue !== "undefined" && numericalValue)
		{
			$("#rnInput").val(this.Numerical2Roman(numericalValue));
		}else{
			this.showAlert("Please insert value for conversion.", "alert alert-info");
		}
	},

	Numerical2Roman : function(numericalValue) {
		  
		var result = '';
		
		if (numericalValue > 3999 || numericalValue < 1 || !$.isNumeric(numericalValue) || Math.floor(numericalValue) != Math.ceil(numericalValue))
		{
			this.showAlert("Please insert integer between 1 and 3999.", "alert alert-danger");
			return '';
		}		

		var number = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
		var roman = ["M", "CM","D","CD","C", "XC", "L", "XL", "X","IX","V","IV","I"];
		for (var i = 0;i<=number.length;i++) {
			while (numericalValue%number[i] < numericalValue) {     
		    result += roman[i];
		      numericalValue -= number[i];
		    }
		}
		   
		  return result;
	},

	Roman2Numerical : function(romanNumeral) {  
	  var result = 0;
	  var romanValue = romanNumeral.toUpperCase();
	  
	  var specialCheckMsg = this.checkSpecialCase(romanValue);
	  if (specialCheckMsg.length > 0){
		  this.showAlert( specialCheckMsg, "alert alert-danger");
		  return "";
	  }
	  
	  var i = 0;
	  while (i < romanValue.length) {
		  var letter = romanValue.charAt(i);        // roman numeral at current position in string.
		  var number = this.letterToNumber(letter);  // Numerical equivalent of letter.
	     
		  if (number < 0)
		  {
			  this.showAlert("Illegal character found in Roman Numeral.", "alert alert-danger");
			  return "";
		  }
	        
	     i++;  // Move on to next position in the string
	     
	     if (i === romanValue.length) {
	           // There is no letter in the string following the current letter =>add the number corresponding to the single letter to result.
	        result += number;
	     }else {
	         // if the next letter in the string has a larger Roman numeral than number
	    	 // => the two letters are counted together as a Roman numeral with value (nextNumber - number).
	        var nextNumber = this.letterToNumber(romanValue.charAt(i));
	        if (nextNumber > number) {
	           // Combine the two letters to get one value, and move on to next position in string.
	           result += (nextNumber - number);
	           i++;
	        }
	        else {
	              // add the value of the one letter onto the number.
	           result += number;
	        }
	     }	     
	  }
	  
	  if (result > 3999)
	  {
		  this.showAlert("Roman numeral must have value 3999 or less.", "alert alert-danger");
		  return "";
	  }
	  
	  return result;
	},

	checkSpecialCase : function(strValue)
	{			 
		if (strValue.indexOf("IIII") > -1
				|| strValue.indexOf("VV") > -1
				|| strValue.indexOf("XXXX") > -1
				|| strValue.indexOf("LL") > -1
				|| strValue.indexOf("CCCC") > -1
				|| strValue.indexOf("DD") > -1)
			return "Please insert correct Roman Numeral.";
		else
			return "";
	},
	
	letterToNumber : function(letter) {
	    // Find the integer value of letter (Upper case) considered as a Roman numeral.  
	    // Return -1 if letter is not a valid Roman numeral.  
		switch (letter) {
	    	case 'I':  return 1;
	    	case 'V':  return 5;
	    	case 'X':  return 10;
	    	case 'L':  return 50;
	    	case 'C':  return 100;
	    	case 'D':  return 500;
	    	case 'M':  return 1000;
	    	default:   return -1;
		}
	},

	loadRnConverter : function()
	{	 
		$("#divRnConverterContent").show();
		$("#divPhoneBookContent").hide();
		
		$("#tabPhoneBook").removeClass("active");
		$("#tabRNConverter").addClass("active");
	},

	showAlert : function(message, alertClass)
	{
		$("#divMessage").show();
		$("#divMessage").removeClass();
		$("#divMessage").addClass(alertClass);
		$("#divMessage").html(message);
	}
	
		
}