<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" href="#" id="tabRNConverter">Roman Numeral Converter</a>
  </li>
  <li class="nav-item" style="display:none">
    <a class="nav-link" href="#" id="tabPhoneBook">Phone Book</a>
  </li>
</ul>

<div class="container">
	<div id="divRnConverterContent">		
		<div class="content-container">
		
			<div id="divMessage">			   
			</div>
			 
			<div class="input-group mb-3">
				<div class="input-group-prepend">
			    	<span class="input-group-text">Roman Numeral</span>
			  	</div>
			  	<input type="text" class="form-control roman-input text-uppercase" placeholder="<M,D,C,L,X,V,I>" id="rnInput">
			  	<div class="input-group-append" id="divClearRoman">
				    <span class="input-group-text">x</span>				    
				</div>
			</div>
			
			<div class="input-group mb-3">
				<button type="button" class="btn btn-default btn-sm" id="btnRnConvert">
					&#8593;&#8595;
		        </button>
			</div>
			
			<div class="input-group mb-3">
				<div class="input-group-prepend">
			    	<span class="input-group-text">Numerical Value</span>
			  	</div>
			  	<input type="text" class="form-control" placeholder="<insert integer between 1 and 3999>"  id="numericalInput">
			  	<div class="input-group-append" id="divClearNumber">
				    <span class="input-group-text">x</span>				    
				</div>
			</div>
			
			<button type="button" class="btn btn-warning" id="btnReset">Reset</button>
			</div>
		</div>
	</div>
	
	<div>
		<div class="content-container author-info"> 
			<p class="font-italic">submitted by Andrea Wong</p>
		</div>
	</div>
	
	
</div>