$(function() {	 
	codingtest.loadOverview(); 	 
	
	$("#navOverview").click(function(){    	
		codingtest.loadOverview(); 	 
	});
    $("#navUtilities").click(function(){    	
    	codingtest.loadUtilities();
    });
    
});

var codingtest = {
	
	loadOverview : function()
	{
		$("#divContent").empty();
		$("#divContent").load("overview");
	 
		this.updateActiveNav($("#navOverview"));	 	
	},

	loadUtilities : function()
	{
		$("#divContent").empty();
		$("#divContent").load("utilities");
		
		$.getScript("js/rnconverter.js");
		
		currentActiveTab = $("#tabRNConverter");
		this.updateActiveNav($("#navUtilities"));
	 
		$("#divRnConverterContent").show();
		$("#divPhoneBookContent").hide();
	},

	updateActiveNav : function(newActiveNav)
	{
		$("#navModules li a").removeClass("active");
		newActiveNav.addClass("active");	
	}
}



