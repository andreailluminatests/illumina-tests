package codingtests.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

	@Value("${main.title}")
	private String mainTitle = "";
	
	@GetMapping("/")
	public String main(Map<String, Object> model) {
		model.put("title", this.mainTitle);
		return "main";
	}
	
	@GetMapping("/utilities")
	public String showUtilities(Map<String, Object> model) {
		return "utilities";
	}
	
	@GetMapping("/overview")
	public String showOverview(Map<String, Object> model) {
		return "overview";
	}
}
